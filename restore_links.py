# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 18:37:30 2016

@author: Felix Pöge

Purpose of this program is to find broken links in Zotero and to automatically
fix them as good as possible.
"""

from pyzotero import zotero
import os
import logging
import json


def search_file(path, search_places):
    """
    Search for an alternative location of a path, if the path destination does
    not exist. The input variable *search_places* has to contain an array of
    possible locations. The first time this method encounters a file which
    cannot be found, it will build a search index to find future entries fast.
    Depending on the number of files, this can take a while.
    Note that the program strictly only takes filenames with previously
    specified extensions into account. This definition is in the configuration
    file.

    As Zotero has problems to handle slashes in paths under Windows, all
    slashes are replaced with the operating system-specific path separator.
    """
    # If it is just a slash-backslash problem, fix it.
    if os.path.exists(path) and (path != path.replace("/", os.sep)):
        return [path.replace("/", os.sep)]

    global search_index
    # Make sure that the search_index object exists.
    try:
        if search_index is None:
            pass
    except NameError as e:
        search_index = None

    if search_index is None:
        print("Building up the search index. This may take a while ...")
        build_index = True
        search_index = {}
    else:
        build_index = False

    (path, filename) = os.path.split(unicode(path))
    fileending = os.path.splitext(filename)[1]

    # Preparing for return values
    toreturn = []

    # Analyze the index, if it already exists.
    if not build_index:
        # Check if the entry is in the index
        for search_place in search_places:
            # If the filename is contained, add it to the return list.
            if search_place in search_index:
                if fileending in search_index[search_place]:
                    if filename in search_index[search_place][fileending]:
                        toreturn.extend(
                            search_index[search_place][fileending][filename])

        # Return whatever was found.
        return toreturn

    # The search index does not exist, build it.
    else:
        for search_place in search_places:
            # Initialize the sub-variables.
            if search_place not in search_index:
                search_index[search_place] = {}
                for ending in index_extensions:
                    search_index[search_place][ending] = {}
                print("-> Processing %s" % search_place)

            # Iterate over all files.
            for dirpath, dirnames, filenames in os.walk(search_place):
                for curfname in filenames:
                    try:
                        curfname = unicode(curfname)
                    except UnicodeDecodeError as e:
                        print(curfname, e)
                        xxx

                    # Complete, proper path and file extension.
                    curpath = os.path.join(dirpath,
                                           curfname).replace("/", os.sep)
                    curending = os.path.splitext(curfname)[1]

                    # When building up the search index, do so.
                    # If more than one location for a file is found, that is
                    # fine, too.
                    if curending in index_extensions:
                        if curfname not in \
                                search_index[search_place][curending]:
                            search_index[search_place][curending][curfname] \
                                = []
                        search_index[search_place][curending][curfname] \
                            .append(curpath)

                    # Check if the entry fits the search criterion. If so, add
                    # it to the list of files to be returned.
                    if curfname == filename:
                        toreturn.append(curpath)
        return toreturn


def _perform_update(item, newpath):
    """
    Update an item in the Zotero repository to a new path.
    """
    logging.info("Updating %s to %s" % (path, newpath))
    item['data']['path'] = newpath
    if zot.update_item(item):
        print("Updating successful")
        return True
    else:
        print("Updating was not successful.")
        return False


def update_file(item):
    """
    Use a Zotero item object (dict type) which contains a path attribute and
    check whether that path is proper and exists. If not, search for an
    alternative location and if found, update the path in the Zotero
    repository.
    If more than one alternative location was found, defer the choice to the
    end of the execution and let the user decide.
    """
    path = unicode(item['data']['path'])
    if os.path.exists(path) and (path == path.replace("/", os.sep)):
        print("Updating was not necessary.")
        return True
    else:
        logging.info("Trying to find the file %s" % (path))
        newpath = search_file(path, search_paths)
    # Exactly one result found, update.
    if len(newpath) == 1:
        newpath = newpath[0]
        return _perform_update(item, newpath)
    # Zero results found, no updating.
    elif len(newpath) == 0:
        print("No alternative location found (Path: %s)." % (path))
        return False
    # More than one result found. Defer for later, ask for user choice.
    else:
        print("More than one location found. You will be asked about this " +
              "again at the end of the program.")
        global user_choices
        # Make sure that the search_index object exists.
        try:
            if user_choices is None:
                user_choices = []
        except NameError:
            user_choices = []
        user_choices.append([item, newpath])
        return False


if __name__ == '__main__':
    # Read the configuration file, set all settings.
    with open("configuration.json") as f:
        # Ignore comments.
        json_content = ""
        for line in f.readlines():
            if len(line) == 1:
                json_content += line + "\n"
            elif line.strip()[0:2] != "//":
                json_content += line + "\n"
        config = json.loads(json_content)

    api_key = config['api_key']
    library_id = config['library_id']
    library_type = config['library_type']
    log_path = config['log_path']
    search_paths = config['search_paths']
    index_extensions = config['index_extensions']

    # Initialize basic services, logging and Zotero client.
    # Create the path to the log file if it does not exist yet.
    log_pathonly = os.path.split(log_path)[0]
    if not os.path.exists(log_pathonly):
        os.makedirs(log_pathonly)
    logging.basicConfig(filename=log_path,
                        level=logging.DEBUG)
    zot = zotero.Zotero(library_id, library_type, api_key)

    # While there are items to process, iterate over them and try to find
    # a file corresponding to the nae in the file system.
    position = 0
    increment = 50
    num_item = 0

    # Items with empty paths
    empty_paths = []

    while True:
        res = zot.items(itemType='attachment',
                        limit=increment,
                        start=position)
        if len(res) == 0:
            break
        print("... %d more items ..." % len(res))
        position += increment
        for item in res:
            num_item += 1
            if 'data' in item:
                if 'path' in item['data']:
                    path = unicode(item['data']['path'])
                    exists = os.path.exists(path) \
                        and (path.replace("/", os.sep) == path)
                    if exists:
                        print("%d: Ok -> %s" % (num_item, path))
                        continue
                    else:
                        print("%d: NOT OK -> %s" % (num_item, path))
                    if path.strip() == '':
                        empty_paths.append(item['data']['title'])
                    result = update_file(item)
                    if result:
                        (pathonly, filename) = os.path.split(path)
                        print("Successful. (%s)" % filename)
                    else:
                        print("Unsuccessful.")
                    print("")
                elif 'linkMode' in item['data']:
                    print("%d: URL to website, no update required (%s)."
                          % (num_item, item['data']['title']))
                else:
                    print("%d: No path found." % num_item)
                    empty_paths.append(item)
            else:
                print("No data found")

    # Handle the cases where more than one file was found.
    try:
        for (item, choices) in user_choices:
            # As the user which item should be selected.
            valid_result = False
            while valid_result is not True:
                print(("When searching for the item with previous path %s, " +
                       "multiple results were found.") %
                      (item['data']['path']))
                i = 0
                for choice in choices:
                    print("[%d]: %s" % (i, choice))
                    i += 1
                print("[%d]: Skip this item, do not update." % (i))
                action = raw_input("Which new path should be selected?\n")
                try:
                    action = int(action)
                except ValueError as e:
                    print("Please enter a number.")
                    continue
                if action >= 0 and action <= i:
                    valid_result = True
            # Do the updating, if the user has selected a choice.
            if action == i:
                continue
            else:
                if _perform_update(item, choices[action]):
                    print("Successful.")
                else:
                    print("Unsuccessful.")
        user_choices = []
    except NameError as e:
        pass
