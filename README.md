# README #

### What is this repository for? ###

This repository contains a simple Python program which attempts to update links to files in a Zotero (https://www.zotero.org/) library.

Presently it is very raw code, comments are very welcome.

### How do I get set up? ###

* Follow the instructions on [how to install PyZotero](https://pyzotero.readthedocs.io/en/latest/#getting-started-short-version)